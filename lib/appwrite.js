import { Account, Avatars, Client, Databases, ID, Query } from "react-native-appwrite";

export const config = {
  endpoint: "https://cloud.appwrite.io/v1",
  plateform: "com.afsal.aora",
  projectId: "663218030015b42b29ce",
  databaseId: "663219590009e54e8711",
  userCollectionId: "6632197c0036bcaa7e47",
  videoCollectionId: "663219ab0037bbf63e22",
  storageId: "66321b760025689fd1e8",
};

// Init your react-native SDK
const client = new Client();

client
  .setEndpoint(config.endpoint) // Your Appwrite Endpoint
  .setProject(config.projectId) // Your project ID
  .setPlatform(config.plateform); // Your application ID or bundle ID.

const account = new Account(client);
const avatars = new Avatars(client);
const databases = new Databases(client);

// create user
export const createUser = async (username, email, password) => {
  try {
    const newAccount = await account.create(
        ID.unique(),
        email, password, username
    )

    if (!newAccount) {
        throw new Error
    }
    const avatarUrl = avatars.getInitials(username)

    const newUser = await databases.createDocument(
        config.databaseId, config.userCollectionId, ID.unique(), {
            accountId: newAccount.$id,
            email: email, 
            avatar: avatarUrl,
            username: username,
        }
    )

    return newUser
  } catch (error) {
    console.log(error)
    throw new Error(error)
  }
};

export async function signIn(email, password) {
  console.log(email, password)
    try {
        const session = await account.createEmailSession(email, password)

        return session
    } catch (error) {
        throw new Error(err)
    }
}

export const getCurrentUser = async () => {
  try {
    const currentAccount = await account.get()

    if (!currentAccount) throw Error

    const currentUser = await databases.listDocuments(
      config.databaseId,
      config.userCollectionId,
      [Query.equal('accountId', currentAccount.$id)]
    )

    if (!currentUser) throw Error

    return currentUser.documents[0];
  } catch (err) {
    console.log(err)
  }
}